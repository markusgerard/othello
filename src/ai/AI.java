package ai;

import java.util.ArrayList;
import java.util.Random;

import othello.*;

public class AI {
	private int color;
	
	public AI(int color) {
		this.color = color;
		
	}
	
	public Move nextMoveRandom(Board board) {
		ArrayList<Move> moves = board.checkValidMove(color);
		int nbOfMoves = moves.size();
		Random rand = new Random();
		if(!moves.isEmpty()) {
			int randIndex = rand.nextInt(nbOfMoves);
			return moves.get(randIndex);
		}
		else {
			System.out.println("No valid moves for AI");
			return new Move(3,3);
		}
	}
	public Move nextMoveTemp(Board board) {
		ArrayList<Move> moves = board.checkValidMove(color);
		if(!moves.isEmpty()) {
			return moves.get(0);
		}
		else {
			System.out.println("No valid moves for AI");
			return new Move(3,3);
		}
	}
	
	public Move nextMove(Board board) {
		ArrayList<Move> moves = board.checkValidMove(color);
		MinMax mm = new MinMax(board, moves, color, 2);
		return mm.miniMax();
	}
}

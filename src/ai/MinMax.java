package ai;

import java.util.ArrayList;

import othello.*;

public class MinMax {
	private Board board;
	private int color;
	private ArrayList<Move> moves;
	private int depth;
	Move bestMove;
	private int bestCount;

	public MinMax(Board board,ArrayList<Move> moves,int color, int depth) {
		this.board= board.clone();
		this.color=color;	
		this.moves=moves;
		this.depth=depth;
		//miniMax(board, moves, color, depth);
	}
	
	public Move miniMax() {
			for(int i = 0; i < moves.size(); i++) {
				int move = bestMove(board,moves,color,depth-1);
				if(move==bestCount)
					bestMove = moves.get(i);
			}
				
			return bestMove;
	}

	private int bestMove(Board board, ArrayList<Move> moves, int currentColor, int depth) {
	//	System.out.println(depth);
		Board localBoard = new Board();
		localBoard = board.clone();
		int nextColor = 1-currentColor;
		int count = Integer.MIN_VALUE;	
		
		if(!localBoard.gameOver() && (depth == 0)) { 
			if(localBoard.getChipsNew(currentColor)>bestCount) {
				bestCount=count;
			}
			return localBoard.getChipsNew(currentColor);
		}
		if(currentColor == color) {
				for(int i = 0; i < moves.size(); i++) {
					Move currentMove = moves.get(i);	
					localBoard.placeChip(currentColor, currentMove.getI(), currentMove.getJ());
					localBoard.replaceChip(currentMove, currentColor);
						if(localBoard.getChipsNew(color)-localBoard.getChipsNew(nextColor)>count)
							count = (localBoard.getChipsNew(color)-localBoard.getChipsNew(nextColor));
						bestMove(localBoard, localBoard.checkValidMove(nextColor), nextColor, depth-1);	
						if(count>=bestCount) {
							bestCount=count;
							bestMove=currentMove ;
						}
				}
				}else {
				count = Integer.MAX_VALUE;
				for(int i = 0; i < moves.size(); i++) {
					Move currentMove = moves.get(i);	
					localBoard.placeChip(currentColor, currentMove.getI(), currentMove.getJ());
					localBoard.replaceChip(currentMove, currentColor);
						if(localBoard.getChipsNew(color)-localBoard.getChipsNew(currentColor)<count)
							count = (localBoard.getChipsNew(color)-localBoard.getChipsNew(currentColor));
						bestMove(localBoard, localBoard.checkValidMove(currentColor), currentColor, depth-1);	
						if(count>bestCount) {
							bestCount=count;
							bestMove=currentMove ;
						}
				}
				}
		return bestCount;
	}
}			
//			System.out.println("------Mini "+"#moves: "+ moves.size());
//			count = Integer.MIN_VALUE;
//			for(int i = 0; i < moves.size(); i++) {
//				System.out.println("LOOP IN ELSE");
//				Board currentBoard = localBoard;
//				Move currentMove = moves.get(i);
//				int row = currentMove.getI();
//				int col = currentMove.getJ();
//				localBoard.placeChip(currentColor, row, col);
//				localBoard.replaceChip(currentMove, currentColor);
//				int score = localBoard.getChipsNew(currentColor);
//				System.out.println("MINIMIZE " + score + "MOVE=  row: " + row + "col: "+ col);
//				if(score > count) {
//					count=score;
//					bestMove=currentMove;
//					bestMove(currentBoard, currentBoard.checkValidMove(nextColor), nextColor, depth-1);
//				}else {
//					bestMove(currentBoard, currentBoard.checkValidMove(nextColor), nextColor, depth-1);
//					System.out.println("ELSE after move in minimize");
//				}
//			}
//		
//		}
//		System.out.println("RETURN EXITING minimize");
//		return bestMove;
//	



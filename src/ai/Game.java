package ai;

import othello.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by pmunoz on 01/04/14. Updated by zwodnik on 09/05/14. Updated by
 * aeap and jgeorge on 14/05/14.
 */

public class Game {

	private Board board = new Board(); // creates the board
	private Player[] players = new Player[2]; // array of players for two
												// players
	private Turn turn; // creates a turn

	private int playerColor;
	private int aiColor;
	private AI ai;
	private AI aiBAD;
	public int winner;

	/**
	 * Driver method for the program. Starts the game.
	 */
	public static void main(String[] args) throws IOException {
		new Game().startGame();
	}

	/**
	 * Starts the game and initializes it with: the first player, a chosen turn and
	 * the board.
	 * 
	 * @throws IOException
	 */
	public void startGame() throws IOException /* the game starts */ {

		int who = this.initPlayers(); // initializes the first player
		this.turn = new Turn((who) % 2); // initializes the turn

		System.out.print("Choose color: ('BLACK' or 'WHITE')" + "\n");
		BufferedReader line = new BufferedReader(new InputStreamReader(System.in));
		String name = line.readLine();
		if (name.equals("BLACK")) {
			playerColor = 1;
			aiColor = 0;
			players[0].setNames("Player");
			players[1].setNames("AI");
		} else if (name.equals("WHITE")) {
			playerColor = 0;
			aiColor = 1;
			players[0].setNames("AI");
			players[1].setNames("D�lig AI");
		}
		ai = new AI(aiColor);
		aiBAD = new AI(playerColor);

		System.out.println(players[0].getName() + " moves"); // indicates who
																// has to play,
																// starting with
																// the player
																// whose turn it
																// is
		this.players[turn.getTurn()].findCanSelect(); // finds the possible
														// moves for the player
														// playing
		
		board.display(); // displays board

		 while (!board.gameOver()) { // when not GameOver, find the possible
			 						// moves from the player
		//for (int ass = 0; ass < 3; ass++) {
			int count = 0; // count of possible moves
			for (int j = 0; j < Board.NUM; j++)
				// search the entire board
				for (int i = 0; i < Board.NUM; i++)
					if (board.findLegalMoveNew(new Move(i, j), ((turn.getTurn()+1)%2)) == true) {
						count++; // add a possible move to the count
					}

			if (count == 0) { // when no possible moves
				System.out.println("count �r 0, currentplayer:" + turn.getTurn());
				turn.change(); // change the turn to the other player
				board.display(); // display the updated board
				count = 0; // reset count to 0
			}

			else {
				if (turn.getTurn() == ((playerColor + 1) % 2)) {
					System.out.println("D�LIG AI �ver i ifsats");
					Move move = aiBAD.nextMoveRandom(board);
					//Move move = ai.nextMoveTemp(board); // creates a new move
					if (board.canSelect(move)) { // if move valid
						this.players[turn.getTurn()].placeChip(move.getI(), move.getJ()); // place
																							// the
																							// chip
						System.out.println("D�LIG AI placing at row: " + move.getI() + ", col: " + move.getJ());
						turn.change(); // change the turn to the other player
					}

					this.players[turn.getTurn()].findCanSelect(); // find the
																	// possible
																	// moves at the
																	// location
					board.display(); 	// display updated board with possible moves
					System.out.println(players[turn.getTurn()].getName() + " moves"); // indicates
																						// who
																						// has
																						// to
																						// play
					
					
					
//					System.out.println("Player �ver i ifsats");
//					int row = this.readRow(); // prompts the player for the row
//												// wanted
//					int col = this.readCol(); // prompts the player for the column
//												// wanted
//
//					Move move = new Move(row, col); // creates a new move
//					if (board.canSelect(move)) { // if move valid
//						this.players[turn.getTurn()].placeChip(row, col); // place
//																			// the
//																			// chip
//						turn.change(); // change the turn to the other player
//					}
//
//					this.players[turn.getTurn()].findCanSelect(); // find the
//																	// possible
//																	// moves at the
//																	// location
//					board.display(); // display updated board with possible moves
//					System.out.println(players[turn.getTurn()].getName() + " moves"); // indicates
//																						// who
//																						// has
//																						// to
//																						// play
				} else {
					System.out.println("AI �ver i ifsats");
					Move move = ai.nextMove(board);
					//Move move = ai.nextMoveTemp(board); // creates a new move
					if (board.canSelect(move)) { // if move valid
						this.players[turn.getTurn()].placeChip(move.getI(), move.getJ()); // place
																							// the
																							// chip
						System.out.println("AI placing at row: " + move.getI() + ", col: " + move.getJ());
						turn.change(); // change the turn to the other player
					}

					this.players[turn.getTurn()].findCanSelect(); // find the
																	// possible
																	// moves at the
																	// location
					board.display(); 	// display updated board with possible moves
					System.out.println(players[turn.getTurn()].getName() + " moves"); // indicates
																						// who
																						// has
																						// to
																						// play
				}
			}
		}
		 int playerScore = board.getChipsNew(playerColor);
		 int aiScore = board.getChipsNew(aiColor);
		 if(playerScore > aiScore) {
		//	 System.out.println("Player wins "+playerScore+"-"+aiScore );
			 System.out.println("D�lig AI wins "+playerScore+"-"+aiScore );
			 winner = playerColor;
		 }
		 else if(playerScore < aiScore) {
			 System.out.println("AI wins "+aiScore+"-"+playerScore );
			 winner = aiColor;
		 }
		 else {
			 System.out.println("Game tied "+aiScore+"-"+playerScore );
		 }
		 System.out.println("Game over");
	}

	/**
	 * Creates two players.
	 * 
	 * @return 1 if black starts and 0 if white starts
	 */
	private int initPlayers() {
		this.players[0] = new Player("name 1", 1, this.board); // player
																// 1
		this.players[1] = new Player("name 2", 0, this.board); // player
																// 2

		return 0;

	}

	/**
	 * Reads the row for the move wanted.
	 * 
	 * @return the value of the row wanted
	 */
	private int readRow() {
		System.out.print("Select a row: "); // Displays a message asking to
											// select a row

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // prompts
																					// for
																					// input
																					// of
																					// row
		Integer value = -1;
		try {
			value = Integer.parseInt(br.readLine()); // reads the value selected
		} catch (IOException e) {
		}
		return value; // returns the value selected
	}

	/**
	 * Reads the column of the move wanted.
	 * 
	 * @return the value of the column wanted
	 */
	private int readCol() {
		System.out.print("Select a column: "); // Displays a message asking to
												// select a column

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // prompts
																					// for
																					// input
																					// of
																					// column
		Integer value = -1;
		try {
			value = Integer.parseInt(br.readLine()); // reads the value selected
		} catch (IOException e) {

		}
		return value; // returns value selected
	}

}
